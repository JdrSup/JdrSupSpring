package com.kenobi.jdrsup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.kenobi.jdrsup.dao.HeroDAO;
import com.kenobi.jdrsup.dao.JDRDAO;
import com.kenobi.jdrsup.dao.ProfessionDAO;
import com.kenobi.jdrsup.dao.RaceDAO;
import com.kenobi.jdrsup.dao.SkillDAO;
import com.kenobi.jdrsup.dao.SkillHeroDAO;
import com.kenobi.jdrsup.dao.StatisticDAO;
import com.kenobi.jdrsup.dao.StatisticHeroDAO;
import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.managers.HeroManager;
import com.kenobi.jdrsup.managers.JDRManager;
import com.kenobi.jdrsup.managers.ProfessionManager;
import com.kenobi.jdrsup.managers.RaceManager;
import com.kenobi.jdrsup.managers.SkillHeroManager;
import com.kenobi.jdrsup.managers.SkillManager;
import com.kenobi.jdrsup.managers.StatisticHeroManager;
import com.kenobi.jdrsup.managers.StatisticManager;
import com.kenobi.jdrsup.managers.interfaces.base.IBaseManager;
import com.kenobi.jdrsup.models.Hero;
import com.kenobi.jdrsup.models.JDR;
import com.kenobi.jdrsup.models.Profession;
import com.kenobi.jdrsup.models.Race;
import com.kenobi.jdrsup.models.Skill;
import com.kenobi.jdrsup.models.SkillHero;
import com.kenobi.jdrsup.models.Statistic;
import com.kenobi.jdrsup.models.StatisticHero;


@SpringBootApplication
public class JdrSupApplication {
		/**
		 * Application entry point
		 * 
		 */
		@Bean
		   public WebMvcConfigurer corsConfigurer() {
		       return new WebMvcConfigurerAdapter() {
		           @Override
		           public void addCorsMappings(CorsRegistry registry) {
		               registry.addMapping("/**/*")
		               		.allowedMethods("HEAD", "PUT", "DELETE", "GET", "POST")
		               		.allowedHeaders("Content-Type")
		               		.allowedOrigins("http://localhost:4200")
		               		.allowCredentials(false)
	               		;
		           }
		       };
		   }
		
		public static void main(String[] args) {
			SpringApplication.run(JdrSupApplication.class, args);
		}

		
		@Bean
		public IBaseManager<Hero> getHeroManager(){
			return new HeroManager();
		}
		
		@Bean
		public IBaseManager<Statistic> getStatisticManager(){
			return new StatisticManager();
		}
		
		@Bean
		public IBaseManager<JDR> getJDRManager(){
			return new JDRManager();
		}
		
		@Bean
		public IBaseManager<Skill> getSkillManager(){
			return new SkillManager();
		}
		
		@Bean
		public IBaseManager<Race> getRaceManager(){
			return new RaceManager();
		}
		
		@Bean
		public IBaseManager<Profession> getProfessionManager(){
			return new ProfessionManager();
		}
		
		@Bean
		public IBaseManager<StatisticHero> getStatisticHeroManager(){
			return new StatisticHeroManager();
		}
		
		@Bean
		public IBaseManager<SkillHero> getSkillHeroManager(){
			return new SkillHeroManager();
		}
		
		
		@Bean
		public IBaseDAO<Hero> getHeroDAO(){
			return new HeroDAO();
		}
		
		@Bean
		public IBaseDAO<Statistic> getStatisticDAO(){
			return new StatisticDAO();
		}
		
		@Bean
		public IBaseDAO<JDR> getJDRDAO(){
			return new JDRDAO();
		}
		
		@Bean
		public IBaseDAO<Skill> getSkillDAO(){
			return new SkillDAO();
		}
		
		@Bean
		public IBaseDAO<Race> getRaceDAO(){
			return new RaceDAO();
		}
		
		@Bean
		public IBaseDAO<Profession> getProfessionDAO(){
			return new ProfessionDAO();
		}
		
		@Bean
		public IBaseDAO<StatisticHero> getStatisticHeroDAO(){
			return new StatisticHeroDAO();
		}
		
		@Bean
		public IBaseDAO<SkillHero> getSkillHeroDAO(){
			return new SkillHeroDAO();
		}
}
