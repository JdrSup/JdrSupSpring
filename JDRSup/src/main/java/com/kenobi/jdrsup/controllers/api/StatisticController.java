package com.kenobi.jdrsup.controllers.api;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kenobi.jdrsup.managers.interfaces.base.IBaseManager;
import com.kenobi.jdrsup.models.Statistic;

@RestController
@RequestMapping("/statistic")
public class StatisticController {
	@Autowired
    private IBaseManager<Statistic> manager;

	/**
	 * List of all statistics
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<Statistic> getAll() {
    	 
  
        return this.manager.getAll();
    }

	/**
	 * Get a statistic by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Statistic get(@PathVariable Integer id, HttpServletResponse response) {
        Statistic entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a statistic
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public Statistic delete(@PathVariable Integer id) {
        Statistic generalType = this.manager.getById(id);

        if (generalType != null) {
            this.manager.delete(generalType);
        }

        return generalType;
    }

	/**
	 * Create a statistic
	 * 
	 * @param
	 */
    @RequestMapping(value="/", method=RequestMethod.POST)
    public Statistic create(@RequestParam String name) {
        Statistic entity = new Statistic();

        entity.setName(name);

        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a statistic
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public Statistic update(HttpServletResponse response, @PathVariable int id, @RequestParam String name) {
        Statistic entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(name != null && !name.equals(entity.getName())) {
            entity.setName(name);

            this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }

}
