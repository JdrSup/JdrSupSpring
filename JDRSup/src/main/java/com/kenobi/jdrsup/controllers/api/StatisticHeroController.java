package com.kenobi.jdrsup.controllers.api;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kenobi.jdrsup.managers.interfaces.base.IBaseManager;
import com.kenobi.jdrsup.models.StatisticHero;

@RestController
@RequestMapping("/statisticHero")
public class StatisticHeroController {
	@Autowired
    private IBaseManager<StatisticHero> manager;

	/**
	 * List of all hero's statistics
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<StatisticHero> getAll() {
    	 
  
        return this.manager.getAll();
    }

	/**
	 * Get a hero's statistic by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public StatisticHero get(@PathVariable Integer id, HttpServletResponse response) {
        StatisticHero entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a hero's statistic
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public StatisticHero delete(@PathVariable Integer id) {
        StatisticHero generalType = this.manager.getById(id);

        if (generalType != null) {
            this.manager.delete(generalType);
        }

        return generalType;
    }

	/**
	 * Create a hero's statistic
	 * 
	 * @param
	 */
    @RequestMapping(value="/", method=RequestMethod.POST)
    public StatisticHero create(@RequestParam int points) {
        StatisticHero entity = new StatisticHero();

        entity.setPoints(points);

        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a hero's statistic
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public StatisticHero update(HttpServletResponse response, @PathVariable int id, @RequestParam int points) {
        StatisticHero entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(points!=entity.getPoints()) {
            entity.setPoints(points);

            this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }

}
