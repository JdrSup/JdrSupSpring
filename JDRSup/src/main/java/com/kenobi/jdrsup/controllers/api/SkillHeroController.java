package com.kenobi.jdrsup.controllers.api;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kenobi.jdrsup.managers.interfaces.base.IBaseManager;
import com.kenobi.jdrsup.models.SkillHero;

@RestController
@RequestMapping("/skillHero")
public class SkillHeroController {
	@Autowired
    private IBaseManager<SkillHero> manager;

	/**
	 * List of all hero's skills
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<SkillHero> getAll() {
    	 
  
        return this.manager.getAll();
    }

	/**
	 * Get a hero's skill by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public SkillHero get(@PathVariable Integer id, HttpServletResponse response) {
        SkillHero entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a hero's skill
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public SkillHero delete(@PathVariable Integer id) {
        SkillHero generalType = this.manager.getById(id);

        if (generalType != null) {
            this.manager.delete(generalType);
        }

        return generalType;
    }

	/**
	 * Create a hero's skill
	 * 
	 * @param
	 */
    @RequestMapping(value="/", method=RequestMethod.POST)
    public SkillHero create(@RequestParam Integer level) {
        SkillHero entity = new SkillHero();

        entity.setLevel(level);

        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a hero's skill
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public SkillHero update(HttpServletResponse response, @PathVariable int id, @RequestParam Integer level) {
        SkillHero entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(level != null && !level.equals(entity.getLevel())) {
            entity.setLevel(level);

            this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }

}
