package com.kenobi.jdrsup.managers;

import com.kenobi.jdrsup.managers.base.BaseManager;
import com.kenobi.jdrsup.managers.interfaces.IHeroManager;
import com.kenobi.jdrsup.models.Hero;

public class HeroManager extends BaseManager<Hero> implements IHeroManager{

}
