package com.kenobi.jdrsup.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "statistic_hero")
public class StatisticHero extends BaseEntity {

	@Column
	private int points;
	
	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

}
