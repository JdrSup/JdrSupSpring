package com.kenobi.jdrsup.models;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kenobi.jdrsup.models.base.BaseEntity;

@Entity
@Table(name = "statistic")
public class Statistic extends BaseEntity {

	@Column
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
