package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.ISkillHeroDAO;
import com.kenobi.jdrsup.models.SkillHero;

@Transactional
public class SkillHeroDAO extends BaseDAO<SkillHero> implements ISkillHeroDAO{

	public SkillHeroDAO() {
		super(SkillHero.class);
	}

}
