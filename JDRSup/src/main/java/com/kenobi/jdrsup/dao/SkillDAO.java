package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.ISkillDAO;
import com.kenobi.jdrsup.models.Skill;
@Transactional
public class SkillDAO extends BaseDAO<Skill> implements ISkillDAO{

	public SkillDAO() {
		super(Skill.class);
	}

}
