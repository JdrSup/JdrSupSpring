package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IStatisticDAO;
import com.kenobi.jdrsup.models.Statistic;
@Transactional
public class StatisticDAO extends BaseDAO<Statistic> implements IStatisticDAO{

	public StatisticDAO() {
		super(Statistic.class);
	}

}
