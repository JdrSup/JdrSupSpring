package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IJDRDAO;
import com.kenobi.jdrsup.models.JDR;
@Transactional
public class JDRDAO extends BaseDAO<JDR> implements IJDRDAO{

	public JDRDAO() {
		super(JDR.class);
	}

}
