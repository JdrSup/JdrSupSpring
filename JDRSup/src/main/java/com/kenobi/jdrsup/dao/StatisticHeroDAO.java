package com.kenobi.jdrsup.dao;

import org.springframework.transaction.annotation.Transactional;

import com.kenobi.jdrsup.dao.base.BaseDAO;
import com.kenobi.jdrsup.dao.interfaces.IStatisticHeroDAO;
import com.kenobi.jdrsup.models.StatisticHero;

@Transactional
public class StatisticHeroDAO extends BaseDAO<StatisticHero> implements IStatisticHeroDAO{

	public StatisticHeroDAO() {
		super(StatisticHero.class);
	}

}
