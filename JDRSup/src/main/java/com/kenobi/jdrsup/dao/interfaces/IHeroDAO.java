package com.kenobi.jdrsup.dao.interfaces;

import com.kenobi.jdrsup.dao.interfaces.base.IBaseDAO;
import com.kenobi.jdrsup.models.Hero;

public interface IHeroDAO extends IBaseDAO<Hero>{

}
